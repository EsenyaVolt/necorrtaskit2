﻿//{{NO_DEPENDENCIES}}
// Включаемый файл, созданный в Microsoft Visual C++.
// Используется necorrtask2.rc
//
#define IDD_NECORRTASK2_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_A1                          1000
#define IDC_A2                          1001
#define IDC_A3                          1002
#define IDC_b1                          1003
#define IDC_b2                          1004
#define IDC_b3                          1005
#define IDC_A4                          1006
#define IDC_A5                          1007
#define IDC_b4                          1008
#define IDC_b5                          1009
#define IDC_t01                         1010
#define IDC_t02                         1011
#define IDC_t03                         1012
#define IDC_t04                         1013
#define IDC_t05                         1014
#define IDC_DLINASIGN                   1015
#define IDC_SIGNALPOWER                 1018
#define IDC_SIGNAL                      1019
#define IDC_MODUL                       1020
#define SPECTR                          1021
#define IDC_SPECTR                      1021
#define IDC_ALGORITM                    1022
#define IDC_ERROR                       1023
#define IDC_SDVIG                       1024
#define IDC_SDVIG2                      1025
#define IDC_OTRAZ                       1025
#define IDC_ALGORITM2                   1026
#define IDC_VYPOLN                      1026

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1025
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
