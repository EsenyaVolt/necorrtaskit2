﻿
// necorrtask2Dlg.h: файл заголовка
//

#pragma once


// Диалоговое окно Cnecorrtask2Dlg
class Cnecorrtask2Dlg : public CDialogEx
{
// Создание
public:
	Cnecorrtask2Dlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NECORRTASK2_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV
	

// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	CWnd* PicWnd;
	CDC* PicDc;
	CRect Pic;

	CWnd* PicWndMod;
	CDC* PicDcMod;
	CRect PicMod;



	//Переменные для работы с масштабом
	double xp = 0, yp = 0,			//коэфициенты пересчета
		xmin = 0, xmax = 0,			//максисимальное и минимальное значение х 
		ymin = 0, ymax = 0;			//максисимальное и минимальное значение y

	double xpmod = 0, ypmod = 0,			//коэфициенты пересчета
		xminmod = 0, xmaxmod = 0,			//максисимальное и минимальное значение х 
		yminmod = 0, ymaxmod = 0;			//максисимальное и минимальное значение y

	//объявление ручек
	CPen osi_pen;		// ручка для осей
	CPen setka_pen;		// для сетки
	CPen signal_pen;		// для графика функции
	CPen signal2_pen;


public:
	double A1;
	double A2;
	double A3;
	double A4;
	double A5;
	double sigma1;
	double sigma2;
	double sigma3;
	double sigma4;
	double sigma5;
	int t0_1;
	int t0_2;
	int t0_3;
	int t0_4;
	int t0_5;
	int t_sign;
	CString Error;

	CButton sdvig;
	CButton otrazhenie;

	float* Signal = new float[t_sign];
	float* Spectr = new float[t_sign];
	float* VosstSignal = new float[t_sign];

	afx_msg double signal(int t);
	afx_msg void Pererisovka();
	afx_msg void PererisovkaDC();
	afx_msg double Psi();
	afx_msg void SdvigOtraz();
	afx_msg void Fienup();

	afx_msg void OnBnClickedSignalpower();
	afx_msg void OnBnClickedSpectr();
	afx_msg void OnBnClickedAlgoritm();

	typedef struct cmplx { float real; float image; } Cmplx;
	//========================================================

	afx_msg void Cnecorrtask2Dlg::fourea(struct cmplx* data, int n, int is)
	{
		int i, j, istep;
		int m, mmax;
		float r, r1, theta, w_r, w_i, temp_r, temp_i;
		float pi = 3.1415926f;

		r = pi * is;
		j = 0;
		for (i = 0; i < n; i++)
		{
			if (i < j)
			{
				temp_r = data[j].real;
				temp_i = data[j].image;
				data[j].real = data[i].real;
				data[j].image = data[i].image;
				data[i].real = temp_r;
				data[i].image = temp_i;
			}
			m = n >> 1;
			while (j >= m) { j -= m; m = (m + 1) / 2; }
			j += m;
		}
		mmax = 1;
		while (mmax < n)
		{
			istep = mmax << 1;
			r1 = r / (float)mmax;
			for (m = 0; m < mmax; m++)
			{
				theta = r1 * m;
				w_r = (float)cos((double)theta);
				w_i = (float)sin((double)theta);
				for (i = m; i < n; i += istep)
				{
					j = i + mmax;
					temp_r = w_r * data[j].real - w_i * data[j].image;
					temp_i = w_r * data[j].image + w_i * data[j].real;
					data[j].real = data[i].real - temp_r;
					data[j].image = data[i].image - temp_i;
					data[i].real += temp_r;
					data[i].image += temp_i;
				}
			}
			mmax = istep;
		}
		if (is > 0)
			for (i = 0; i < n; i++)
			{
				data[i].real /= (float)n;
				data[i].image /= (float)n;
			}

	}

	cmplx* spectr = new cmplx[t_sign];
	
	afx_msg void OnBnClickedVypoln();
};
