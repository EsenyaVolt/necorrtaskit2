﻿
// necorrtask2.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// Cnecorrtask2App:
// Сведения о реализации этого класса: necorrtask2.cpp
//

class Cnecorrtask2App : public CWinApp
{
public:
	Cnecorrtask2App();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern Cnecorrtask2App theApp;
