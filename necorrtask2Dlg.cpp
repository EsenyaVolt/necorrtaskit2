﻿
// necorrtask2Dlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "necorrtask2.h"
#include "necorrtask2Dlg.h"
#include "afxdialogex.h"

#include <fstream>
#include <random>

#define PI 3.1415926535

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

using namespace std;

#define DOTS(x,y) (xp*((x)-xmin)),(yp*((y)-ymax)) // макрос перевода координат для графика сигнала
#define DOTSMOD(x,y) (xpmod*((x)-xminmod)),(ypmod*((y)-ymaxmod)) // макрос перевода координат для графика сигнал+шум

// Диалоговое окно Cnecorrtask2Dlg

Cnecorrtask2Dlg::Cnecorrtask2Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NECORRTASK2_DIALOG, pParent)
	, A1(5)
	, A2(3)
	, A3(1)
	, A4(2)
	, A5(4)
	, sigma1(2)
	, sigma2(3)
	, sigma3(2)
	, sigma4(4)
	, sigma5(2)
	, t0_1(100)
	, t0_2(350)
	, t0_3(600)
	, t0_4(840)
	, t0_5(960)
	, t_sign(1024)
	, Error(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void Cnecorrtask2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_A1, A1);
	DDX_Text(pDX, IDC_A2, A2);
	DDX_Text(pDX, IDC_A3, A3);
	DDX_Text(pDX, IDC_A4, A4);
	DDX_Text(pDX, IDC_A5, A5);
	DDX_Text(pDX, IDC_b1, sigma1);
	DDX_Text(pDX, IDC_b2, sigma2);
	DDX_Text(pDX, IDC_b3, sigma3);
	DDX_Text(pDX, IDC_b4, sigma4);
	DDX_Text(pDX, IDC_b5, sigma5);
	DDX_Text(pDX, IDC_t01, t0_1);
	DDX_Text(pDX, IDC_t02, t0_2);
	DDX_Text(pDX, IDC_t03, t0_3);
	DDX_Text(pDX, IDC_t04, t0_4);
	DDX_Text(pDX, IDC_t05, t0_5);
	DDX_Text(pDX, IDC_DLINASIGN, t_sign);
	DDX_Text(pDX, IDC_ERROR, Error);
	DDX_Control(pDX, IDC_SDVIG, sdvig);
	DDX_Control(pDX, IDC_OTRAZ, otrazhenie);
}

BEGIN_MESSAGE_MAP(Cnecorrtask2Dlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SIGNALPOWER, &Cnecorrtask2Dlg::OnBnClickedSignalpower)
	ON_BN_CLICKED(IDC_SPECTR, &Cnecorrtask2Dlg::OnBnClickedSpectr)
	ON_BN_CLICKED(IDC_ALGORITM, &Cnecorrtask2Dlg::OnBnClickedAlgoritm)
	ON_BN_CLICKED(IDC_VYPOLN, &Cnecorrtask2Dlg::OnBnClickedVypoln)
END_MESSAGE_MAP()


// Обработчики сообщений Cnecorrtask2Dlg

BOOL Cnecorrtask2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию
	//для  картинки
	PicWnd = GetDlgItem(IDC_SIGNAL);
	PicDc = PicWnd->GetDC();
	PicWnd->GetClientRect(&Pic);

	PicWndMod= GetDlgItem(IDC_MODUL);
	PicDcMod = PicWndMod->GetDC();
	PicWndMod->GetClientRect(&PicMod);

	CButton* pcb1 = (CButton*)(this->GetDlgItem(IDC_SDVIG));
	pcb1->SetCheck(1);

	// перья
	setka_pen.CreatePen(		//для сетки
		PS_DOT,					//пунктирная
		1,						//толщина 1 пиксель
		RGB(0, 0, 0));			//цвет  черный

	osi_pen.CreatePen(			//координатные оси
		PS_SOLID,				//сплошная линия
		3,						//толщина 3 пикселя
		RGB(0, 0, 0));			//цвет черный

	signal_pen.CreatePen(			//график исходного сигнала
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(0, 0, 255));			//цвет синий

	signal2_pen.CreatePen(			//график восстановленного сигнала
		PS_SOLID,				//сплошная линия
		2,						//толщина 2 пикселя
		RGB(255, 0, 0));		//цвет red


	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void Cnecorrtask2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();

		Pererisovka();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR Cnecorrtask2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void Cnecorrtask2Dlg::Pererisovka()
{

	PicDc->FillSolidRect(&Pic, RGB(250, 250, 250));			//закрашиваю фон 
	PicDcMod->FillSolidRect(&PicMod, RGB(250, 250, 250));			//закрашиваю фон 


	//ГРАФИК СИГНАЛА

		//область построения
	xmin = -30;			//минимальное значение х
	xmax = t_sign - 1;			//максимальное значение х
	ymin = -0.5;			//минимальное значение y
	ymax = 6;		//максимальное значение y

	/*Mashtab(s, t_sign, ymin, ymax);*/

	xp = ((double)(Pic.Width()) / (xmax - xmin));			//Коэффициенты пересчёта координат по Х
	yp = -((double)(Pic.Height()) / (ymax - ymin));			//Коэффициенты пересчёта координат по У


	PicDc->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDc->MoveTo(DOTS(0, ymax));
	PicDc->LineTo(DOTS(0, ymin));
	//создаём Ось Х
	PicDc->MoveTo(DOTS(xmin, 0));
	PicDc->LineTo(DOTS(xmax, 0));

	//подпись осей
	PicDc->TextOutW(DOTS(10, ymax - 0.5), _T("S"));
	PicDc->TextOutW(DOTS(xmax - 20, 0.5), _T("t"));


	PicDc->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (float x = 0; x <= xmax; x += xmax * 0.12)
	{
		PicDc->MoveTo(DOTS(x, ymax));
		PicDc->LineTo(DOTS(x, ymin));
	}
	//отрисовка сетки по у
	for (float y = 0; y <= ymax; y++)
	{
		PicDc->MoveTo(DOTS(xmin, y));
		PicDc->LineTo(DOTS(xmax, y));
	}


	//подпись точек на оси
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc->SelectObject(font);

	//по Y с шагом 
	for (double i = 0; i <= ymax; i++)
	{
		CString str;
		str.Format(_T("%.0f"), i);
		PicDc->TextOutW(DOTS(-20, i), str);
	}
	//по X с шагом 
	for (double j = xmax * 0.12; j <= xmax; j += xmax * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDc->TextOutW(DOTS(j, -0.2), str);
	}

	//ГРАФИК ШУМ+СИГНАЛ

	//область построения
	xminmod = -30;			//минимальное значение х
	xmaxmod =  t_sign-1;			//максимальное значение х
	yminmod = -5;			//минимальное значение y
	ymaxmod = 60;		//максимальное значение y

	xpmod = ((double)(PicMod.Width()) / (xmaxmod - xminmod));			//Коэффициенты пересчёта координат по Х
	ypmod = -((double)(PicMod.Height()) / (ymaxmod - yminmod));			//Коэффициенты пересчёта координат по У


	PicDcMod->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDcMod->MoveTo(DOTSMOD(0, ymaxmod));
	PicDcMod->LineTo(DOTSMOD(0, yminmod));
	//создаём Ось Х
	PicDcMod->MoveTo(DOTSMOD(xminmod, 0));
	PicDcMod->LineTo(DOTSMOD(xmaxmod, 0));

	//подпись осей
	PicDcMod->TextOutW(DOTSMOD(15, ymaxmod - 0.5), _T("A"));
	PicDcMod->TextOutW(DOTSMOD(xmaxmod - 20, 5), _T("t"));


	PicDcMod->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (float x = 0; x <= xmaxmod; x += xmaxmod * 0.12)
	{
		PicDcMod->MoveTo(DOTSMOD(x, ymaxmod));
		PicDcMod->LineTo(DOTSMOD(x, yminmod));
	}
	//отрисовка сетки по у
	for (float y = ymaxmod * 0.24; y <= ymaxmod; y += ymaxmod * 0.24)
	{
		PicDcMod->MoveTo(DOTSMOD(xminmod, y));
		PicDcMod->LineTo(DOTSMOD(xmaxmod, y));
	}


	//подпись точек на оси
	CFont fontmod;
	fontmod.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDcMod->SelectObject(fontmod);

	//по Y с шагом 10
	for (double i = ymaxmod * 0.12; i <= ymaxmod; i += ymaxmod * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), i);
		PicDcMod->TextOutW(DOTSMOD(-23, i), str);
	}
	//по X с шагом 25
	for (double j = 0; j <= xmaxmod; j += xmaxmod * 0.12)
	{
		CString str;
		str.Format(_T("%.2f"), j/t_sign);
		PicDcMod->TextOutW(DOTSMOD(j, -1), str);
	}
}

void Cnecorrtask2Dlg::PererisovkaDC()
{

	PicDc->FillSolidRect(&Pic, RGB(250, 250, 250));			//закрашиваю фон 


	//ГРАФИК СИГНАЛА

		//область построения
	xmin = -30;			//минимальное значение х
	xmax = t_sign - 1;			//максимальное значение х
	ymin = -0.5;			//минимальное значение y
	ymax = 6;		//максимальное значение y

	/*Mashtab(s, t_sign, ymin, ymax);*/

	xp = ((double)(Pic.Width()) / (xmax - xmin));			//Коэффициенты пересчёта координат по Х
	yp = -((double)(Pic.Height()) / (ymax - ymin));			//Коэффициенты пересчёта координат по У


	PicDc->SelectObject(&osi_pen);		//выбираем перо

	//создаём Ось Y
	PicDc->MoveTo(DOTS(0, ymax));
	PicDc->LineTo(DOTS(0, ymin));
	//создаём Ось Х
	PicDc->MoveTo(DOTS(xmin, 0));
	PicDc->LineTo(DOTS(xmax, 0));

	//подпись осей
	PicDc->TextOutW(DOTS(10, ymax - 0.5), _T("S"));
	PicDc->TextOutW(DOTS(xmax - 20, 0.5), _T("t"));


	PicDc->SelectObject(&setka_pen);

	//отрисовка сетки по х
	for (float x = 0; x <= xmax; x += xmax * 0.12)
	{
		PicDc->MoveTo(DOTS(x, ymax));
		PicDc->LineTo(DOTS(x, ymin));
	}
	//отрисовка сетки по у
	for (float y = 0; y <= ymax; y++)
	{
		PicDc->MoveTo(DOTS(xmin, y));
		PicDc->LineTo(DOTS(xmax, y));
	}


	//подпись точек на оси
	CFont font;
	font.CreateFontW(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS || CLIP_LH_ANGLES, DEFAULT_QUALITY, DEFAULT_PITCH, _T("Times New Roman"));
	PicDc->SelectObject(font);

	//по Y с шагом 
	for (double i = 0; i <= ymax; i++)
	{
		CString str;
		str.Format(_T("%.0f"), i);
		PicDc->TextOutW(DOTS(-20, i), str);
	}
	//по X с шагом 
	for (double j = xmax * 0.12; j <= xmax; j += xmax * 0.12)
	{
		CString str;
		str.Format(_T("%.0f"), j);
		PicDc->TextOutW(DOTS(j, -0.2), str);
	}
}

double Cnecorrtask2Dlg::signal(int t)
{
	double A[] = { A1, A2, A3,A4,A5 };
	double t0[] = { t0_1, t0_2, t0_3, t0_4,t0_5 };
	double del[] = { sigma1, sigma2, sigma3,sigma4,sigma5 };
	double s = 0;

	for (int j = 0; j <= 4; j++)
	{
		s += A[j] * exp(-(((t - t0[j]) / del[j]) * ((t - t0[j]) / del[j])));
	}

	return s;
}

double Cnecorrtask2Dlg::Psi()		//рандомизация для шума
{
	float r = 0;
	float a = 0.;
	float b = 2 * PI;

	for (int i = 1; i <= 12; i++)
	{
		r += (double)rand() / (double)RAND_MAX * (b - a) + a;	// [0;2PI]
	}
	return r / 12;
}

void Cnecorrtask2Dlg::OnBnClickedSignalpower()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData(TRUE);
	Pererisovka();

	float* ish_sign = new float[t_sign];
	float norm = 0.;

	for (int i = 0; i < t_sign; i++)
	{
		ish_sign[i] = signal(i);	//СЧИТАЕМ ИСХОДНЫЙ СИГНАЛ
	}

	for (int i = 0; i < t_sign; i++)
	{
		Signal[i] = ish_sign[i];
	}

	PicDc->SelectObject(&signal_pen);
	PicDc->MoveTo(DOTS(0, Signal[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDc->LineTo(DOTS(i, Signal[i]));		//СТРОИМ ИСХОДНЫЙ СИГНАЛ
	}

	delete ish_sign;
}


void Cnecorrtask2Dlg::OnBnClickedSpectr()
{
	// TODO: добавьте свой код обработчика уведомлений

	/*НАЧИНАЕМ ОБРАБАТЫВАТЬ ЗАШУМЛЕННЫЙ СИГНАЛ С ПОМОЩЬЮ БЫСТРОГО ПРЕОБРАЗОВАНИЯ ФУРЬЕ*/

	int is = -1;			//ПРЯМОЕ
	const int mass = t_sign;

	for (int i = 0; i < mass; i++)		//СОЗДАЕМ КОМПЛЕКСНЫЙ МАССИВ, С НУЛЕВОЙ МНИМОЙ ЧАСТЬЮ
	{
		spectr[i].real = Signal[i];
		spectr[i].image = 0;
	}

	fourea(spectr, t_sign, is);

	for (int i = 0; i < mass; i++)
	{
		Spectr[i] = sqrt((spectr[i].real) * (spectr[i].real) + (spectr[i].image) * (spectr[i].image));			//РАСЧЕТ СПЕКТРА
	}

	PicDcMod->SelectObject(&signal_pen);
	PicDcMod->MoveTo(DOTSMOD(0, Spectr[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDcMod->LineTo(DOTSMOD(i, Spectr[i]));			//СТРОИМ СПЕКТР
	}
}

void Cnecorrtask2Dlg::SdvigOtraz()
{
	float* tempmas1 = new float[t_sign];
	for (int i = 0; i < t_sign; i++)
		tempmas1[i] = VosstSignal[i];
	for (int i = 0; i < t_sign / 2; i++)
	{
		VosstSignal[i] = tempmas1[t_sign / 2 - i - 1];
	}
	for (int i = 0; i < t_sign / 2; i++)
		VosstSignal[t_sign / 2 + i] = tempmas1[t_sign - i - 1];

	delete tempmas1;
}

void Cnecorrtask2Dlg::Fienup()
{
	int is = 0;
	int j = 0;

	cmplx* Rez = new cmplx[t_sign];
	float* vosstSignal = new float[t_sign];

	float  TAU = 0.0000001; // Точность вычислений

	while (1)
	{
		j++;

		if (j == 1) // если первая итерация
		{
			float* phase0 = new float[t_sign];

			for (int i = 0; i < t_sign; i++)
			{
				phase0[i] = Psi();
			}

			cmplx* ExpPhase0 = new cmplx[t_sign];

			for (int i = 0; i < t_sign; i++)
			{
				ExpPhase0[i].real = cos(2 * PI * phase0[i]);
				ExpPhase0[i].image = sin(2 * PI * phase0[i]);
			}

			for (int i = 0; i < t_sign; i++)
			{
				Rez[i].real = Spectr[i] * ExpPhase0[i].real;
				Rez[i].image = Spectr[i] * ExpPhase0[i].image;
				vosstSignal[i] = Rez[i].real;
			}

			delete phase0;
			delete ExpPhase0;
		}

		// для остальных итераций
		is = 1;
		fourea(Rez, t_sign, is);

		for (int i = 0; i < t_sign; i++)
		{
			if (Rez[i].real < 0)		Rez[i].real = 0;
			VosstSignal[i] = Rez[i].real;
			Rez[i].image = 0;
		}

		Sleep(25);

		is = -1;
		fourea(Rez, t_sign, is);

		float* phase = new float[t_sign];

		for (int i = 0; i < t_sign; i++)
		{
			phase[i] = atan2(Rez[i].image, Rez[i].real);
			Rez[i].real = Spectr[i] * cos(phase[i]);
			Rez[i].image = Spectr[i] * sin(phase[i]);
		}

		PererisovkaDC();

		PicDc->SelectObject(&signal_pen);
		PicDc->MoveTo(DOTS(0, Signal[0]));

		for (int i = 0; i < t_sign; i++)
		{
			PicDc->LineTo(DOTS(i, Signal[i]));		//СТРОИМ ИСХОДНЫЙ СИГНАЛ
		}

		PicDc->SelectObject(&signal2_pen);
		PicDc->MoveTo(DOTS(0, VosstSignal[0]));

		for (int i = 0; i < t_sign; i++)
		{
			PicDc->LineTo(DOTS(i, VosstSignal[i]));			//СТРОИМ 
		}

		float nevyaz = 0;
		for (int i = 0; i < t_sign; i++)
		{
			nevyaz += sqrt((vosstSignal[i] - VosstSignal[i]) * (vosstSignal[i] - VosstSignal[i]));
			vosstSignal[i] = VosstSignal[i];
		}

		if ((nevyaz / t_sign) <= TAU) 
		{
			break;
		}

		CString err = NULL;
		err.Format(L"%.7f", nevyaz / t_sign);
		Error = err;
		UpdateData(FALSE);

		delete phase;
	}
	//delete Rez;
	//delete vosstSignal;
}

void Cnecorrtask2Dlg::OnBnClickedAlgoritm()
{
	// TODO: добавьте свой код обработчика уведомлений

	Fienup();

	MessageBox(L"Процесс восстановления завершен.", L"Результат", MB_ICONSTOP | MB_OK);
}

void Cnecorrtask2Dlg::OnBnClickedVypoln()
{
	 //TODO: добавьте свой код обработчика уведомлений
	if (otrazhenie.GetCheck() == BST_CHECKED)
	{
		SdvigOtraz();
	}

	if (sdvig.GetCheck() == BST_CHECKED)
	{
		// для сдвига  восстановленного сигнала
		float* MassVrem = new float[t_sign];
		float* VosstHelp = new float[t_sign];
		float* MassNew = new float[t_sign];
		float* MassInvNew = new float[t_sign];

		for (int i = 0; i < t_sign; i++)
		{
			VosstHelp[i] = VosstSignal[i];
			MassVrem[i] = VosstSignal[i];
			MassNew[i] = 0;
			MassInvNew[i] = 0;
		}

		for (int j = 0; j < t_sign; j++)
		{
			for (int i = 0; i < t_sign; i++)
			{
				if ((i + j) >= t_sign)
					VosstHelp[i] = MassVrem[i + j - t_sign];
				else
					VosstHelp[i] = MassVrem[i + j];
			}

			for (int k = 0; k < t_sign; k++)
			{
				MassNew[j] += sqrt((Signal[k] - VosstHelp[k]) * (Signal[k] - VosstHelp[k]));
			}

		}

		SdvigOtraz();

		for (int i = 0; i < t_sign; i++)
		{
			VosstHelp[i] = VosstSignal[i];
			MassVrem[i] = VosstSignal[i];
		}

		for (int j = 0; j < t_sign; j++)
		{
			for (int i = 0; i < t_sign; i++)
			{
				if ((i + j) >= t_sign)
					VosstHelp[i] = MassVrem[i + j - t_sign];
				else
					VosstHelp[i] = MassVrem[i + j];
			}
			for (int k = 0; k < t_sign; k++)
			{
				MassInvNew[j] += sqrt((Signal[k] - VosstHelp[k]) * (Signal[k] - VosstHelp[k]));
			}
		}

		SdvigOtraz();

		double mininv = MassInvNew[0];
		double min = MassNew[0];
		int min1 = 0;
		int min2 = 0;


		for (int i = 1; i < t_sign; i++)
		{
			if (MassNew[i] < min)
			{
				min = MassNew[i];
				min1 = i;
			}

			if (MassInvNew[i] < mininv)
			{
				mininv = MassInvNew[i];
				min2 = i;
			}
		}

		if (mininv < min)
		{
			SdvigOtraz();

			for (int i = 0; i < t_sign; i++)
			{
				MassVrem[i] = VosstSignal[i];
			}

			for (int i = 0; i < t_sign; i++)
			{
				if ((i + min2) >= t_sign)
				{
					VosstSignal[i] = MassVrem[i + min2 - t_sign];
				}
				else
				{
					VosstSignal[i] = MassVrem[i + min2];
				}
			}
		}
		else
		{
			for (int i = 0; i < t_sign; i++)
			{
				MassVrem[i] = VosstSignal[i];
			}

			for (int i = 0; i < t_sign; i++)
			{
				if ((i + min1) >= t_sign)
				{
					VosstSignal[i] = MassVrem[i + min1 - t_sign];
				}
				else
				{
					VosstSignal[i] = MassVrem[i + min1];
				}
			}
		}

		delete MassVrem;
		delete VosstHelp;
		delete MassNew;
		delete MassInvNew;
	}

	PererisovkaDC();

	PicDc->SelectObject(&signal_pen);
	PicDc->MoveTo(DOTS(0, Signal[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDc->LineTo(DOTS(i, Signal[i]));		//СТРОИМ ИСХОДНЫЙ СИГНАЛ
	}

	PicDc->SelectObject(&signal2_pen);
	PicDc->MoveTo(DOTS(0, VosstSignal[0]));

	for (int i = 0; i < t_sign; i++)
	{
		PicDc->LineTo(DOTS(i, VosstSignal[i]));			//СТРОИМ 
	}
}
